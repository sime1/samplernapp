import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialTopTabNavigator }
  from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { FAB, Provider, Appbar } from 'react-native-paper';

const Tab = createMaterialTopTabNavigator();

export default function App() {
  const [count, setCount] = useState(0);
  const increment = () => setCount(prev => prev + 1);
  const decrement = () => setCount(prev => prev - 1);

  return (
    <Provider>
      <NavigationContainer>
        <Appbar.Header>
          <Appbar.Content title="Un semplice esempio" />
        </Appbar.Header>
        <Tab.Navigator>
          <Tab.Screen name="Incremento">
            {/*render props per creare il componente della schermata*/}
            {() => <IncrementTab count={count} increment={increment} />}
          </Tab.Screen>
          <Tab.Screen name="Decremento">
            {/*render props */}
            {() => <DecrementTab count={count} decrement={decrement} />}
          </Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

function IncrementTab({ count, increment }) {
  return (
    <View style={styles.container}>
      <Text accessibilityLiveRegion="polite">{count}</Text>
      <FAB
        style={styles.fab}
        icon="plus"
        onPress={increment}
        accessibilityLabel="incrementa"
        accessibilityHint="incrementa il valore del contatore di 1"
      />
    </View>
  );
}

function DecrementTab({ count, decrement }) {
  return (
    <View style={styles.container}>
      <Text accessibilityLiveRegion="polite">{count}</Text>
      <FAB
        style={styles.fab}
        icon="minus"
        onPress={decrement}
        accessibilityLabel="decrementa"
        accessibilityHint="decrementa il valore del contatore di 1"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
